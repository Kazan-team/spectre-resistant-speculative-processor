// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
#![no_std]

extern crate alloc;

pub mod instruction;
pub mod simulator;
