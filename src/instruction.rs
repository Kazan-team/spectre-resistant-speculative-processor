// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
use core::{num::NonZeroU8, ops::Not};

#[derive(Clone, Copy, PartialEq, Eq, Debug, Default)]
#[repr(transparent)]
pub struct Register(pub Option<NonZeroU8>);

impl Register {
    pub fn from_u8(v: u8) -> Self {
        Self(NonZeroU8::new(v))
    }
    pub fn to_u8(self) -> u8 {
        self.0.map(|v| v.get()).unwrap_or(0)
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(u8)]
pub enum CompareCondition {
    IntEqual = 0,
    IntNotEqual = 1,
    UIntLessThan = 2,
    SIntLessThan = 3,
    UIntGreaterThanEqual = 4,
    SIntGreaterThanEqual = 5,
}

macro_rules! compare_condition_eval {
    ($eval_fn:ident, $uint:ident, $sint:ident) => {
        pub fn $eval_fn(self, lhs: $uint, rhs: $uint) -> bool {
            match self {
                CompareCondition::IntEqual => lhs == rhs,
                CompareCondition::IntNotEqual => lhs != rhs,
                CompareCondition::UIntLessThan => lhs < rhs,
                CompareCondition::SIntLessThan => (lhs as $sint) < (rhs as $sint),
                CompareCondition::UIntGreaterThanEqual => lhs >= rhs,
                CompareCondition::SIntGreaterThanEqual => (lhs as $sint) >= (rhs as $sint),
            }
        }
    };
}

impl CompareCondition {
    compare_condition_eval!(eval_8bit, u8, i8);
    compare_condition_eval!(eval_16bit, u16, i16);
    compare_condition_eval!(eval_32bit, u32, i32);
    compare_condition_eval!(eval_64bit, u64, i64);
    compare_condition_eval!(eval_128bit, u128, i128);
}

impl Not for CompareCondition {
    type Output = Self;

    fn not(self) -> Self::Output {
        match self {
            CompareCondition::IntEqual => CompareCondition::IntNotEqual,
            CompareCondition::IntNotEqual => CompareCondition::IntEqual,
            CompareCondition::UIntLessThan => CompareCondition::UIntGreaterThanEqual,
            CompareCondition::SIntLessThan => CompareCondition::SIntGreaterThanEqual,
            CompareCondition::UIntGreaterThanEqual => CompareCondition::UIntLessThan,
            CompareCondition::SIntGreaterThanEqual => CompareCondition::SIntLessThan,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(u8)]
pub enum UnaryAluOp {
    CountLeadingZeros,
    CountTrailingZeros,
    PopulationCount,
    ZeroExtendU8,
    ZeroExtendU16,
    ZeroExtendU32,
    SignExtendS8,
    SignExtendS16,
    SignExtendS32,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(u8)]
pub enum BinaryAluOp {
    CompareIntEqual = CompareCondition::IntEqual as u8,
    CompareIntNotEqual = CompareCondition::IntNotEqual as u8,
    CompareUIntLessThan = CompareCondition::UIntLessThan as u8,
    CompareSIntLessThan = CompareCondition::SIntLessThan as u8,
    CompareUIntGreaterThanEqual = CompareCondition::UIntGreaterThanEqual as u8,
    CompareSIntGreaterThanEqual = CompareCondition::SIntGreaterThanEqual as u8,
    Add,
    Sub,
    Mul,
    /// `dest = ((src1 as u128 * src2 as u128) >> 64) as u64`
    UMulHigh,
    /// `dest = ((src1 as i128 * src2 as i128) >> 64) as u64`
    SMulHigh,
    SDiv,
    UDiv,
    SRem,
    URem,
    And,
    Or,
    Xor,
    ShiftLeft,
    UShiftRight,
    SShiftRight,
    RotateLeft,
    RotateRight,
    /// `dest = ((src1 & 0xFFFFFFFF) << 32) | (src2 & 0xFFFFFFFF)`
    /// used for creating 64-bit immediate
    Pack32,
    /// Generalized bit-Reverse -- like RISC-V `grev` instruction
    GRev,
}

impl BinaryAluOp {
    pub fn make_compare(cond: CompareCondition) -> Self {
        match cond {
            CompareCondition::IntEqual => Self::CompareIntEqual,
            CompareCondition::IntNotEqual => Self::CompareIntNotEqual,
            CompareCondition::UIntLessThan => Self::CompareUIntLessThan,
            CompareCondition::SIntLessThan => Self::CompareSIntLessThan,
            CompareCondition::UIntGreaterThanEqual => Self::CompareUIntGreaterThanEqual,
            CompareCondition::SIntGreaterThanEqual => Self::CompareSIntGreaterThanEqual,
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(u8)]
pub enum TernaryAluOp {
    /// `dest = (src1 * src2) + src3`
    MulAdd,
    /// `dest = (((src1 as u128 * src2 as u128) + src3 as u128) >> 64) as u64`
    UMulAddHigh,
    /// `dest = (((src1 as i128 * src2 as i128) + src3 as i128) >> 64) as u64`
    SMulAddHigh,
    /// `dest = ((src1 as u128 | (src2 as u128 << 64)) / src3 as u128) as u64`
    UDivWide,
    /// `dest = ((src1 as i128 | (src2 as i128 << 64)) / src3 as i128) as u64`
    SDivWide,
    /// `dest = ((src1 as u128 | (src2 as u128 << 64)) % src3 as u128) as u64`
    URemWide,
    /// `dest = ((src1 as i128 | (src2 as i128 << 64)) % src3 as i128) as u64`
    SRemWide,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(u8)]
pub enum LoadStoreOp {
    /// load: `reg0 = ptr::read((reg1 + reg2) as *const u8)`
    /// store: `ptr::write((reg1 + reg2) as *mut u8, reg0)`
    EitherEndian8,
    /// load: `reg0 = u16::from_le_bytes(ptr::read((reg1 + reg2) as *const _))`
    /// store: `ptr::write((reg1 + reg2) as *mut _, (reg0 as u16)::to_le_bytes())`
    LittleEndian16,
    /// load: `reg0 = u32::from_le_bytes(ptr::read((reg1 + reg2) as *const _))`
    /// store: `ptr::write((reg1 + reg2) as *mut _, (reg0 as u32)::to_le_bytes())`
    LittleEndian32,
    /// load: `reg0 = u64::from_le_bytes(ptr::read((reg1 + reg2) as *const _))`
    /// store: `ptr::write((reg1 + reg2) as *mut _, (reg0 as u64)::to_le_bytes())`
    LittleEndian64,
    /// load: `reg0 = u16::from_be_bytes(ptr::read((reg1 + reg2) as *const _))`
    /// store: `ptr::write((reg1 + reg2) as *mut _, (reg0 as u16)::to_be_bytes())`
    BigEndian16,
    /// load: `reg0 = u32::from_be_bytes(ptr::read((reg1 + reg2) as *const _))`
    /// store: `ptr::write((reg1 + reg2) as *mut _, (reg0 as u32)::to_be_bytes())`
    BigEndian32,
    /// load: `reg0 = u64::from_be_bytes(ptr::read((reg1 + reg2) as *const _))`
    /// store: `ptr::write((reg1 + reg2) as *mut _, (reg0 as u64)::to_be_bytes())`
    BigEndian64,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(C, u8)]
pub enum Opcode {
    /// unconditional branch
    Branch,
    /// conditional branch
    CondBranch {
        cond: CompareCondition,
    },
    /// branch to address in register
    BranchReg,
    UnaryAluReg {
        op: UnaryAluOp,
    },
    BinaryAluRegReg {
        op: BinaryAluOp,
    },
    BinaryAluRegImm {
        op: BinaryAluOp,
    },
    BinaryAluImmReg {
        op: BinaryAluOp,
    },
    TernaryAluRegRegReg {
        op: TernaryAluOp,
    },
    LoadSignedRegReg {
        op: LoadStoreOp,
    },
    LoadUnsignedRegReg {
        op: LoadStoreOp,
    },
    LoadSignedRegImm {
        op: LoadStoreOp,
    },
    LoadUnsignedRegImm {
        op: LoadStoreOp,
    },
    StoreRegRegReg {
        op: LoadStoreOp,
    },
    StoreRegRegImm {
        op: LoadStoreOp,
    },
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(C)]
pub struct Regs2And3 {
    pub reg2: Register,
    pub reg3: Register,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(transparent)]
pub struct Regs2And3OrImm {
    bits: i32,
}

impl Regs2And3OrImm {
    pub fn new_imm(value: i32) -> Self {
        Self { bits: (value) }
    }
    pub fn new_regs(regs: Regs2And3) -> Self {
        Self {
            bits: (i32::from_ne_bytes([regs.reg2.to_u8(), regs.reg3.to_u8(), 0, 0])),
        }
    }
    pub fn to_imm(self) -> i32 {
        self.bits
    }
    pub fn to_regs(self) -> Regs2And3 {
        let [reg2, reg3, _, _] = self.bits.to_ne_bytes();
        Regs2And3 {
            reg2: Register::from_u8(reg2),
            reg3: Register::from_u8(reg3),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
#[repr(C)]
pub struct Instruction {
    pub opcode: Opcode,
    pub reg0: Register,
    pub reg1: Register,
    pub regs_or_imm: Regs2And3OrImm,
}
