// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
use alloc::{collections::BTreeSet, rc::Rc, vec::Vec};
use core::cell::Cell;

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Debug)]
#[repr(transparent)]
struct CycleNum(u64);

impl CycleNum {
    fn lsb(self) -> usize {
        self.0 as usize & 1
    }
}

#[derive(Debug)]
struct StateTracker {
    current_cycle_num: Cell<CycleNum>,
    state_changed: Cell<bool>,
}

impl StateTracker {
    fn new() -> Self {
        Self {
            current_cycle_num: Cell::new(CycleNum(0)),
            state_changed: Cell::new(true),
        }
    }
}

pub struct Register<T> {
    state_tracker: Rc<StateTracker>,
    last_updated_cycle_num: CycleNum,
    state: [T; 2],
}

impl<'state_tracker, T> Register<T> {
    pub fn new(simulator: &mut Simulator, value: T) -> Self
    where
        T: Clone,
    {
        let state_tracker = simulator.state_tracker.clone();
        state_tracker.state_changed.set(true);
        Self {
            last_updated_cycle_num: state_tracker.current_cycle_num.get(),
            state_tracker,
            state: [value.clone(), value],
        }
    }
    pub fn current(&self) -> &T {
        if self.last_updated_cycle_num < self.state_tracker.current_cycle_num.get() {
            &self.state[self.last_updated_cycle_num.lsb() ^ 1]
        } else {
            &self.state[self.last_updated_cycle_num.lsb()]
        }
    }
    pub fn set_next(&mut self, value: T)
    where
        T: Clone + Eq,
    {
        let current_cycle_num = self.state_tracker.current_cycle_num.get();
        if self.last_updated_cycle_num != current_cycle_num {
            debug_assert!(self.last_updated_cycle_num < current_cycle_num);
            let current = self.state[self.last_updated_cycle_num.lsb() ^ 1].clone();
            let next = &mut self.state[self.last_updated_cycle_num.lsb()];
            *next = current;
            self.last_updated_cycle_num = current_cycle_num;
        }
        let next = &mut self.state[self.last_updated_cycle_num.lsb()];
        if *next != value {
            *next = value;
            self.state_tracker.state_changed.set(true);
        }
    }
}

pub trait Process {
    fn settle(&self, simulator: &mut Simulator) {
        let _ = simulator;
    }
    fn on_clock_tick(&self, simulator: &mut Simulator) {
        let _ = simulator;
    }
}

pub struct Simulator {
    state_tracker: Rc<StateTracker>,
    processes: Vec<Rc<dyn Process>>,
    processes_set: BTreeSet<*const dyn Process>,
}

impl Simulator {
    /// iterate through processes while allowing new processes to be appended while iterating
    fn for_each_process(&mut self, mut f: impl FnMut(&mut Self, Rc<dyn Process>)) {
        for i in 0.. {
            if let Some(process) = self.processes.get(i).cloned() {
                f(self, process);
            } else {
                break;
            }
        }
    }
    pub fn new() -> Self {
        Self {
            state_tracker: Rc::new(StateTracker::new()),
            processes: Vec::new(),
            processes_set: BTreeSet::new(),
        }
    }
    pub fn force_state_change(&mut self) {
        self.state_tracker.state_changed.set(true);
    }
    pub fn settle(&mut self) {
        for _ in 0..10 {
            if !self.state_tracker.state_changed.replace(false) {
                return;
            }
            self.for_each_process(|this, process| process.settle(this));
        }
        panic!("state did not settle after trying");
    }
    pub fn clock_tick(&mut self) {
        self.settle();
        self.state_tracker
            .current_cycle_num
            .set(CycleNum(self.state_tracker.current_cycle_num.get().0 + 1));
        self.force_state_change();
        self.for_each_process(|this, process| process.on_clock_tick(this));
        self.settle();
    }
    pub fn add_process(&mut self, process: Rc<dyn Process>) -> bool {
        if self.processes_set.insert(Rc::as_ptr(&process)) {
            self.processes.push(process);
            self.force_state_change();
            true
        } else {
            false
        }
    }
}
