WIP speculative processor that is immune (at the level of digital logic) to attacks based on mis-speculated instructions leaking data or changing timing
